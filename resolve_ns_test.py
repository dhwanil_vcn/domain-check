#!/usr/bin/python3
# Note for the next python programmer:
# - the tests are the function in ResolveTests classes
# - frist string after def is the input
# - second string after def is the expected output

""" Unit Test for resolve_ns.py

This script following the examples on https://stackoverflow.com/a/39908502 and
on https://docs.python.org/3/library/unittest.mock.html (search sys.stdout)
"""
import unittest
import resolve_ns
from unittest.mock import patch
import sys
from io import StringIO

class ResolveTests(unittest.TestCase):
    """Tests related to resolve_ns.py

    This Test Unit uses unittest.mock which requires python 3
    """
    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_google(self, mock_stdout):
        """Test known domain name that is not hosted on VCN

        """
        resolve_ns.test_domain("google.com")
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  google.com\n")


    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_google(self, mock_stdout):
        """Test known domain name that is hosted on VCN and does not ends with
            `vcn.bc.ca`

        """
        resolve_ns.test_domain("avalonrecoverysociety.org")
        self.assertEqual(mock_stdout.getvalue(), "")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_vcn(self, mock_stdout):
        """Test known domain name that is hosted on VCN
        """
        resolve_ns.test_domain("vcn.bc.ca")
        self.assertEqual(mock_stdout.getvalue(), "")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_123(self, mock_stdout):
        """Test a number and make sure nothing breaks
        """
        resolve_ns.test_domain("123")
        self.assertEqual(mock_stdout.getvalue(), "Nonexisting query name:  123\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_non_existing(self, mock_stdout):
        """Test a domain name that does not exist
        """
        resolve_ns.test_domain("1234567dasfsdfaaewlfodg.com")
        self.assertEqual(mock_stdout.getvalue(), "Nonexisting query name:  1234567dasfsdfaaewlfodg.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_no_servers(self, mock_stdout):
        """Test domain name with no name servers
        """
        resolve_ns.test_domain("819.com")
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  819.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_empty(self, mock_stdout):
        """Test a string with no characters
        """
        resolve_ns.test_domain("")
        self.assertEqual(mock_stdout.getvalue(), "Nonexisting query name:  \n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_google_domain(self, mock_stdout):
        """Test a subdomain of google.com
        """
        resolve_ns.test_domain("mail.google.com")
        self.assertEqual(mock_stdout.getvalue(), "Contains no answer:      mail.google.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_vcn_domain(self, mock_stdout):
        """Test a subdomain of vcn.bc.ca that should fail
        """
        resolve_ns.test_domain("dsfdsa.vcn.bc.ca")
        self.assertEqual(mock_stdout.getvalue(), "Nonexisting query name:  dsfdsa.vcn.bc.ca\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_empty(self, mock_stdout):
        """Test a string with no characters
        """
        resolve_ns.test_domain("")
        self.assertEqual(mock_stdout.getvalue(), "Nonexisting query name:  \n")

    # Not testing max string with max length b/c it would take all the memory

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_usage(self, mock_stdout):
        """Test to show script usage
        """
        resolve_ns.main([])
        self.assertEqual(mock_stdout.getvalue(),
            "Usage: python resolve_ns \"domain name\"\n" +
	        "Usage: cat \"text file path\" | python resolve_ns\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_single_arg(self, mock_stdout):
        """Test using one argument
        """
        resolve_ns.main(["google.com"])
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  google.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_option_two_args(self, mock_stdout):
        """Test using two arguments
        """
        resolve_ns.main(["google.com", "apple.com"])
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  google.com\nNot in VCN name server:  apple.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    @patch("sys.stdin", StringIO("google.com"))
    def test_main_single_line(self, mock_stdout):
        """Test using file in with one line
        """
        resolve_ns.main([])
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  google.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    @patch("sys.stdin", StringIO("google.com\nyahoo.com"))
    def test_main_two_line(self, mock_stdout):
        """Test using file in with two line
        """
        resolve_ns.main([])
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  google.com\nNot in VCN name server:  yahoo.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    @patch("sys.stdin", StringIO("google.com"))
    def test_main_args_and_lines(self, mock_stdout):
        """Test using both files in and arguments
        """
        resolve_ns.main(["youtube.com"])
        self.assertEqual(mock_stdout.getvalue(), "Not in VCN name server:  google.com\nNot in VCN name server:  youtube.com\n")

    # Not testing max arguments b/c it is limited by OS


if __name__ == '__main__':
    unittest.main();
